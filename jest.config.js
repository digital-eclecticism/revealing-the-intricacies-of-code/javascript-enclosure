module.exports = {
    collectCoverage: true,
    collectCoverageFrom: [
      'cases/*.{js,jsx}'
    ],
    coverageDirectory: 'coverage',
    coverageReporters: ['text', 'json', 'json-summary', 'lcov'],
    coverageThreshold: {
      global: {
        branches: 100,
        functions: 100,
      },
    },
    verbose: true,
  };