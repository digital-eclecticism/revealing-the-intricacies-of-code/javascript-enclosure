
const type = {
  function: "function",
  object: "object",
  string: "string",
  number: "number"
}

// Normal Function declaration has to be declared locally because it will not export in this syntax

function functionNormal() {
  return typeof this;
}

require('./cases/functionGlobal')
require('./cases/functionExpression')
require('./cases/functionArrow')
require('./cases/functionMethod')
require('./cases/functionNew')

// Normal Function Declaration

test('Normal Function Declaration', () => {
  expect(typeof functionNormal).toBe(type.function);
});

test('Encapsulation of the Normal Function Declaration', () => {
  expect(functionNormal()).toBe(type.object);
});

// Global Function

test('Global Function', () => {
  expect(typeof functionGlobal).toBe(type.function);
});

test('Encapsulation of the Global Function', () => {
  expect(functionGlobal()).toBe(type.object);
});

// Function Expression

test('Function Expression', () => {
  expect(typeof functionExpression).toBe(type.function);
});

test('Encapsulation of the Function Expression', () => {
  expect(functionExpression()).toBe(type.object);
});

// Arrow Function

test('Arrow Function', () => {
  expect(typeof functionArrow).toBe(type.function);
});

test('Encapsulation of the Arrow Function', () => {
  expect(functionArrow()).toBe(type.object);
});

// Module Methods

test('Module Methods', () => {
  expect(typeof obj.functionMethod).toBe(type.function);
});

test('Encapsulation of the Module Method', () => {
  expect(obj.functionMethod()).toBe(type.object);
});

// New Function

test('New Function', () => {
  expect(typeof functionNew).toBe(type.function);
});

test('Encapsulation of the New Function', () => {
  expect(functionNew()).toBe(type.object);
});